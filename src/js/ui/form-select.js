const selects = document.querySelectorAll('.js-select');

if(selects.length > 0){
    selects.forEach(select => {
        const choices = new Choices(select, {
            searchEnabled: false,
            removeItemButton: true,
            loadingText: 'Загрузка...',
            noResultsText: 'Результатов не найдено',
            noChoicesText: 'Нет выбора',
            itemSelectText: '',
        });
    });
}