const teamLists = document.querySelectorAll('.team-list');

teamLists.forEach(list => {
    const items = list.querySelectorAll('.team-list__item');

    let plusValue = (items.length - 1) * 11;

    for (let i = 0; i < items.length; i++) {
        items[i].style.left = `${plusValue}px`;
        plusValue -= 11;
    }
});