const   regTrigger = document.querySelector('.js-show-trigger-reg'),
        regBlock = document.querySelector('.js-show-block-reg');

if(regTrigger){
    regTrigger.addEventListener('click', () => {
        regBlock.classList.toggle('invisible');
    });
}

